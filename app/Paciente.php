<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'paciente';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nome', 'email', 'idade',
        'sexo', 'sexo', 'clinica_id'
    ];

    public function atendimentos()
    {
        return $this->hasMany(Atendimento::class);
    }

    public function clinica()
    {
        return $this->belongsTo(Clinica::class);
    }
}
