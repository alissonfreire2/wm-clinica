<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    protected $table = 'medico';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nome', 'email', 'crm',
        'especialidade', 'clinica_id'
    ];

    public function atendimentos()
    {
        return $this->hasMany(Atendimento::class);
    }

    public function clinica()
    {
        return $this->belongsTo(Clinica::class);
    }
}
