<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Atendimento;
use App\Mail\AvisoEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EnviarEmailDeAviso extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:enviaremailaviso';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $atendimentos = Atendimento::where([
            ['data', '=', Carbon::now()->setTimezone('America/Sao_Paulo')->subDays(1)->format('Y-m-d')],
            ['warned_at', '=', null]
        ]);

        // dump([$atendimentos->count(), Carbon::now()->subDays(1)]);
        $atendimentos->each(function ($atendimento) {
            $data = [
                'medico' => $atendimento->medico->nome,
                'data' => (new Carbon($atendimento->data))->format('d/m/Y')
            ];
            Mail::to($atendimento->paciente->email)->send(new AvisoEmail($data));
            sleep(5);

            $atendimento->warned_at = Carbon::now()->format('Y-m-d');
            $atendimento->update();
        });
    }
}
