<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    protected $table = 'clinica';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nome', 'cnes', 'cnpj', 'proprietario'
    ];

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function medicos()
    {
        return $this->hasMany(Medico::class);
    }

    public function pacientes()
    {
        return $this->hasMany(Paciente::class);
    }
}
