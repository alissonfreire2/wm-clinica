<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClinicaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string',
            'cnpj' => 'required|cnpj|unique:clinica,cnpj',
            'cnes' => 'required|string|min:6|max:6|unique:clinica,cnes',
            'proprietario' => 'required|string|max:15',
        ];
    }
}
