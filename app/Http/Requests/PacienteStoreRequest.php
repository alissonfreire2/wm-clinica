<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PacienteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string|max:55',
            'email' => 'required|email|max:55|unique:paciente,email',
            'sexo' => 'required|in:M,F,I',
            'idade' => 'required|integer|min:0',
            'clinica_id' => 'required|exists:clinica,id',
        ];
    }
}
