<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AtendimentoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "data" => 'nullable|date|after_or_equal:today',
            "status" => 'nullable|in:C,A,R',
            "medico_id" => 'nullable|exists:medico,id',
            "paciente_id" => 'nullable|exists:paciente,id'
        ];
    }
}
