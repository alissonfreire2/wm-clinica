<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClinicaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'nullable|string',
            'cnpj' => "nullable|cnpj|unique:clinica,cnpj,{$this->clinica->id},id",
            'cnes' => "nullable|string|min:6|max:6|unique:clinica,cnes,{$this->clinica->id},id",
            'proprietario' => 'nullable|string|max:15',
        ];
    }
}
