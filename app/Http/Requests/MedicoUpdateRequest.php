<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'nullable|string|max:55',
            'email' => "nullable|email|max:55|unique:medico,email,{$this->medico->id},id",
            'crm' => "nullable|string|regex:/^[0-9]{6,10}\/[A-Z]{2}$/|unique:medico,crm,{$this->medico->id},id",
            'especialidade' => 'nullable|string|max:55',
            'clinica_id' => 'nullable|exists:clinica,id',
        ];
    }
}
