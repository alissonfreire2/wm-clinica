<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PacienteUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'nullable|string|max:55',
            'email' => "nullable|email|max:55|unique:paciente,email,{$this->paciente->id},id",
            'sexo' => 'nullable|in:M,F,I',
            'idade' => 'nullable|integer|min:0',
            'clinica_id' => 'nullable|exists:clinica,id',
        ];
    }
}
