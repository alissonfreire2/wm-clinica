<?php

namespace App\Http\Controllers;

use App\Atendimento;
use App\Mail\CanceladoEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\AtendimentoStoreRequest;
use App\Http\Requests\AtendimentoUpdateRequest;

class AtendimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()
            ->json(Atendimento::with('medico', 'paciente')->paginate(20), 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AtendimentoStoreRequest $request)
    {
        $data = $request->only(['status', 'data', 'paciente_id', 'medico_id']);

        $atendimento = Atendimento::create($data);

        return response()->json($atendimento, 201, [], JSON_UNESCAPED_SLASHES);
    }

    /**JSON_PRETTY_PRINT
     * Display the specified resource.
     *
     * @param  \App\Atendimento  $atendimento
     * @return \Illuminate\Http\Response
     */
    public function show(Atendimento $atendimento)
    {
        return response()->json($atendimento->load('medico', 'paciente'), 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Atendimento  $atendimento
     * @return \Illuminate\Http\Response
     */
    public function update(AtendimentoUpdateRequest $request, Atendimento $atendimento)
    {
        $data = $request->only(['status', 'data', 'paciente_id', 'medico_id']);

        $atendimento->update($data);

        if ($data['status'] === 'C') {
            try {
                Mail::to($atendimento->paciente->email)->send(new CanceladoEmail);
            } catch (\Throwable $th) {
                //
            }
        }

        return response()->json($atendimento, 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Atendimento  $atendimento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Atendimento $atendimento)
    {
        //
    }
}
