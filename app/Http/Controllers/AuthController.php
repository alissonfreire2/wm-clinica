<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller
{
    // criar uma auth request
    public function login(Request $request)
    {

        $client_id = config('services.passport.client_id');

        $client_secret = config('services.passport.client_secret');

        $request->request->add([
            'grant_type'    => 'password',
            'client_id'     => $client_id,
            'client_secret' => $client_secret,
            'username' => $request->username,
            'password' => $request->password
        ]);

        $request->headers->set('Content-Type', 'application/json');

        // forward the request to the oauth token request endpoint
        $response = Route::dispatch(Request::create('/oauth/token', 'post'));

        $content = json_decode($response->getContent());

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            $content = [
                'token' => $content->access_token,
                'user' => User::where('email', $request->username)->first()
            ];
        }


        return response()->json($content, $statusCode);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        // return 201
        return response()->json(User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]), 201);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        return response()->json('Logged out successfully', 200);
    }
}
