<?php

namespace App\Http\Controllers;

use App\Clinica;
use App\Http\Requests\ClinicaStoreRequest;
use Illuminate\Http\Request;
use App\Http\Requests\ClinicaUpdateRequest;

class ClinicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Clinica::paginate(20), 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClinicaStoreRequest $request)
    {
        $data = $request->only(['nome', 'proprietario', 'cnes', 'cnpj']);

        $clinica = Clinica::create($data);

        return response()->json($clinica, 201, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function show(Clinica $clinica)
    {
        return response()->json($clinica->load('medicos', 'pacientes'), 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function update(ClinicaUpdateRequest $request, Clinica $clinica)
    {
        $data = $request->only(['nome', 'proprietario', 'cnes', 'cnpj']);

        $clinica->update($data);

        return response()->json($clinica, 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinica $clinica)
    {
        //
    }
}
