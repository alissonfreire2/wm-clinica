<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/login', 'AuthController@login')->name('login');
Route::post('/register', 'AuthController@register')->name('register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'auth:api'], function () {

    Route::resource('clinica', 'ClinicaController');

    Route::resource('medico', 'MedicoController');

    Route::resource('paciente', 'PacienteController');

    Route::resource('atendimento', 'AtendimentoController');

    Route::post('/logout', 'AuthController@logout')->name('logout');
});
