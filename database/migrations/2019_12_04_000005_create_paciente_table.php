<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'paciente';

    /**
     * Run the migrations.
     * @table paciente
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome', 55);
            $table->unsignedInteger('idade');
            $table->enum('sexo', ['M', 'F', 'I']);
            $table->string('email', 55)->unique();
            $table->unsignedInteger('clinica_id');

            $table->foreign('clinica_id')
                ->references('id')->on('clinica')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
