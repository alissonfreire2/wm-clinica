<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtendimentoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'atendimento';

    /**
     * Run the migrations.
     * @table atendimento
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('data');
            $table->enum('status', ['A', 'C', 'R'])->default('A');
            $table->unsignedInteger('medico_id');
            $table->unsignedInteger('paciente_id');

            $table->foreign('medico_id')
                ->references('id')->on('medico')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('paciente_id')
                ->references('id')->on('paciente')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->timestamp('warned_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
