import Vue from 'vue';
import VueRouter from 'vue-router';
import store from "./store";

import Example from "./components/ExampleComponent.vue";
import ClinicaIndex from "./components/clinica/ClinicaIndex.vue";
import ClinicaForm from "./components/clinica/ClinicaForm.vue";
import PacienteIndex from "./components/paciente/PacienteIndex.vue";
import PacienteForm from "./components/paciente/PacienteForm.vue";
import MedicoIndex from "./components/medico/MedicoIndex.vue";
import MedicoForm from "./components/medico/MedicoForm.vue";
import AtendimentoIndex from "./components/atendimento/AtendimentoIndex.vue";
import AtendimentoForm from "./components/atendimento/AtendimentoForm.vue";
import Home from "./components/Home.vue";
import Dashboard from "./components/Dashboard.vue";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/example",
      component: Example
    },
    {
      path: "/dashboard",
      // name: "dashboard",
      component: Dashboard,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: "",
          component: ClinicaIndex
        },
        {
          path: "clinica",
          name: "clinica-index",
          component: ClinicaIndex
        },
        {
          path: "clinica/create",
          name: "clinica-create",
          component: ClinicaForm
        },
        {
          path: "clinica/edit/:id",
          name: "clinica-edit",
          component: ClinicaForm
        },
        {
          path: "paciente",
          name: "paciente-index",
          component: PacienteIndex
        },
        {
          path: "paciente/create",
          name: "paciente-create",
          component: PacienteForm
        },
        {
          path: "paciente/edit/:id",
          name: "paciente-edit",
          component: PacienteForm
        },
        {
          path: "medico",
          name: "medico-index",
          component: MedicoIndex
        },
        {
          path: "medico/create",
          name: "medico-create",
          component: MedicoForm
        },
        {
          path: "medico/edit/:id",
          name: "medico-edit",
          component: MedicoForm
        },
        {
          path: "atendimento",
          name: "atendimento-index",
          component: AtendimentoIndex
        },
        {
          path: "atendimento/create",
          name: "atendimento-create",
          component: AtendimentoForm
        },
        {
          path: "atendimento/edit/:id",
          name: "atendimento-edit",
          component: AtendimentoForm
        }
      ]
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/register",
      name: "register",
      component: Register
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      next("/login");
      return;
    }
  }
  next();
  return;
});

export default router;
