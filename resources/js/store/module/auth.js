import axios from "axios";

export const state = {
  status: "",
  token: localStorage.getItem("token") || "",
  user: {}
};

export const getters = {
  isLoggedIn: state => !!state.token,
  auth_success: state => state.status === 'success',
  auth_status: state => state.status
};

export const mutations = {
  LOGIN (state, { token, user }) {
    state.token = token;
    state.user = user;
  },
  LOGOUT (state) {
    state.status = "";
    state.token = "";
  },
  AUTH_REQUEST (state) {
    state.status = "loading";
  },
  AUTH_SUCCESS (state, token, user) {
    state.status = "success";
    state.token = token;
    state.user = user;
  },
  AUTH_ERROR (state) {
    state.status = "error";
  },
  AUTH_DEFAULT (state) {
    state.status = "";
  }
};

export const actions = {
  login ({ commit }, { email, password }) {
    return new Promise((resolve, reject) => {
      commit("AUTH_REQUEST");
      axios
        .post("/api/login", { username: email, password })
        .then(resp => {
          commit("AUTH_SUCCESS");
          let { token, user } = resp.data;

          token = `Bearer ${token}`;

          localStorage.setItem("token", token);

          axios.defaults.headers.common["Authorization"] = token;
          console.log(axios.defaults.headers.common);
          commit("LOGIN", { token, user });
          resolve(resp);
        })
        .catch(e => {
          commit("AUTH_ERROR");
          localStorage.removeItem("token");
          reject(e);
        });
    });
  },
  register ({ commit }, { name, email, password, password_confirmation }) {
    return new Promise((resolve, reject) => {
      commit("AUTH_REQUEST");
      axios
        .post("/api/register", { name, email, password, password_confirmation })
        .then(resp => {
          commit("AUTH_SUCCESS");
          resolve(resp);
        })
        .catch(e => {
          commit("AUTH_ERROR");
          reject(e);
        });
    });
  },
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      commit("AUTH_REQUEST");
      axios
        .post("/api/logout")
        .then(resp => {
          commit("AUTH_SUCCESS");
          commit("LOGOUT");
          localStorage.removeItem("token");
          delete axios.defaults.headers.common["Authorization"];
          resolve(resp);
        })
        .catch(e => {
          commit("AUTH_ERROR");
          reject(e);
        });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
