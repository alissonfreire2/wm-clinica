/* eslint-disable no-undef */
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('bootstrap')
require("popper.js");
import VueTheMask from "vue-the-mask";
import Vuelidate from "vuelidate";
import VueSweetalert2 from "vue-sweetalert2";
import VTooltip from "v-tooltip";

import axios from 'axios'

import store from "./store";

import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faPen,
  faPlus,
  faTimes,
  faRedo,
  faPaperPlane,
  faEnvelope,
  faKey,
  faEye,
  faEyeSlash,
  faDoorOpen,
  faUser
} from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import "sweetalert2/dist/sweetalert2.min.css";

window.Vue = require('vue');

library.add(
  faPen,
  faPlus,
  faTimes,
  faPaperPlane,
  faRedo,
  faEnvelope,
  faKey,
  faEye,
  faEyeSlash,
  faDoorOpen,
  faUser
);

Vue.component("fa-icon", FontAwesomeIcon);

Vue.use(VueTheMask);

Vue.use(Vuelidate);

Vue.use(VueSweetalert2);

Vue.use(VTooltip);

Vue.use(BootstrapVue);

VTooltip.options.defaultTemplate =
  '<div class="tooltip-vue" role="tooltip"><div class="tooltip-vue-arrow"></div><div class="tooltip-vue-inner"></div></div>';
VTooltip.options.defaultArrowSelector =
  ".tooltip-vue-arrow, .tooltip-vue__arrow";
VTooltip.options.defaultInnerSelector =
  ".tooltip-vue-inner, .tooltip-vue__inner";


// Vue.directive("tooltip", VTooltip.VTooltip);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('wm-app', require('./components/App.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.prototype.$http = axios;
// Vue.prototype.$http.defaults.baseURL = 'localhost:8000/api';
Vue.prototype.$http.defaults.headers.common["Accept"] = "application/json";

const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = token;
}

import router from './routes';

/* eslint-disable no-unused-vars */
const app = new Vue({
  el: "#app",
  router,
  store
});
