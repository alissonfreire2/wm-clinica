@component('mail::message')
# Aviso de cancelamento da consulta

Infelizmente sua consulta foi cancelada.

Agradecemos a compreensão,<br>
{{ config('app.name') }}
@endcomponent
