@component('mail::message')
# Aviso da data da consulta

Sua consulta com o médico: <strong>{{ $data['medico'] }}</strong> está marcada para o dia: <strong>{{ $data['data'] }}</strong>

Agradecemos sua compreensão,<br>
{{ config('app.name') }}
@endcomponent
